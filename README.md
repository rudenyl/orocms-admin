# Overview
OroCMS Admin is a minimalist Laravel package that provides control panel functionality to your Laravel application.
Forked from [Github: rudenyl/orocms-admin](https://github.com/rudenyl/orocms-admin) and enhanced to support Laravel 5.5 _(and above)_ versions.


## Installation
Create or edit existing your ```composer.json```, and add the following:
```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/rudenyl/orocms-admin.git"
        },
        {
            "type": "vcs",
            "url": "https://github.com/rudenyl/menus.git"
        }
    ],
    "minimum-stability": "dev"
}
```

We're then ready to install, add the package wit composer.
```
$ composer require rudenyl/orocms-admin
```


After the package has been installed, add the following service provider in your ```config/app.php```
```
'providers' => [
    OroCMS\Admin\AdminServiceProvider::class,
]
```

## User Model
In your ```config/auth.php```, update the ```model``` value with:
```
'providers' => [
    'users' => [
        'driver' => 'eloquent',
        'model' => OroCMS\Admin\Entities\User::class,
    ],

    // ----------------------------------------------------------
    // The lines below should be commented the properly get the
    // correct User entity. If not, you will get the GenericUser
    // object which will fail (undefined method "is").
    // ----------------------------------------------------------
    // 'users' => [
    //     'driver' => 'database',
    //      'table' => 'users',
    //  ],
],

```

If you want to use the existing ```App\User``` model, you can extend it with the Model that comes with the admin package
```
// app\User.php
namespace App;

class User extends OroCMS\Admin\Entities\User ...
```


## Publishing
To publish the package's config, views and assets, run the following command in you terminal:
```
$ php artisan admin:install
```

This command will copy assets, migration scripts and will attempt to migrate and seed your database.

To do a manual migration command or seed, you can do the following:
```
$ php artisan admin:migrate
$ php artisan admin:seed
```

_See `php artisan` for other **admin** commands._


## Assets
The admin package comes with existing `webpack.mix.js` and additional modules and uses `bower` to install.
To ensure you will be able to compile the assets needed to run the admin package, you need to install the required node modules and bower components. You can skip the bower component install if you these folders (i sometimes symlinked these from somewhere) already.

Install required node packages:
```
$ npm install
```

Install `bower` node module and install with:
```
$ npm install bower --no-save
$ bower install
```

Copy the `webpack.mix.js` from `/vendor/rudenyl/orocms-admin/webpack.mix.js` to your Laravel folder. You may override or copy the mix task options, depending if you already have several task configurations under the default `webpack.mix.js`.

Build assets with:
```
$ npm run dev
```

or, for production deployment
```
$ npm run prod
```


## Running the Application
In your terminal, run
```
$ php artisan serve
```

Point your browser to ```http://localhost:8000/admin```

Credentials: `admin@admin.com / password`

All set!


# Modules/plugins
OroCMS comes with 3rd party modules and plugins capability.
In your `composer.json`, add the following lines:
```
    ...
    "autoload": {
        "psr-4": {
            "App\\": "app/",
            ...
            "Modules\\": "modules/",  // <--- add this
            "Plugins\\": "plugins/"   // <--- add this
        },
        ...
    },
    ...
```

Then issue the command below to reload the class mapping of your vendor class autoloader.
```
$ composer dump-autoload
```

## Module generator
This package comes with a **module generator** to create a base scaffold for you start with. Make sure the `modules` directory is available under your main Laravel installation.

To generate the module, use the following command:
```
$php artisan admin:makemodule -m "Paypal IPN Integration"
```

You should be able to see the specified module under `./modules` created for you :)


See example module at [https://gitlab.com/rudenyl/orocms-module-articles.git](https://gitlab.com/rudenyl/orocms-module-articles.git).


Example plugin is at [https://gitlab.com/rudenyl/orocms-plugin-markdown.git](https://gitlab.com/rudenyl/orocms-plugin-markdown.git).


# Troubleshooting
```
[BadMethodCallException]         
  Call to undefined method insert
```
```
ErrorException (E_ERROR)
Call to undefined method Caffeinated\Menus\Item::attributes()
```
When you get the above errors, it could be that the module fork for ```caffeinated/menus``` in your vendor folder wasn't properly fetched (could be a bad composer repository reference). As a temporary workaround, remove the current /caffeinated/menus folder and drop in and ```git clone https://github.com/rudenyl/menus``` instead.

This issue will show if you're adding the package to your ```composer.json``` and then issuing ```$ composer update```.

or you can do a manual pull:
```
$ cd vendor/caffeinated
$ rm -rf menus
$ git clone https://github.com/rudenyl/menus.git
```

---
```
Class 'Modules\<module-name>\Providers\<module-name>ServiceProvider' not found
```
This is caused by missing class dependency. Make sure you added the `modules` path in your `composer.json` **autoload** section.
```
    ...
    "autoload": {
        "psr-4": {
            "App\\": "app/",
            ...
            "Modules\\": "modules/",  // <--- add this
        },
        ...
    },
    ...
```

Then reload class dependencies with:
```
$ composer dump-autoload
```


# License

This package is open-sourced software licensed under [The BSD 3-Clause License](http://opensource.org/licenses/BSD-3-Clause)
