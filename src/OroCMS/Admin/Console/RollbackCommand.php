<?php
namespace OroCMS\Admin\Console;

use Schema;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RollbackCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:rollback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback the module migration.';

    /**
     * @var OroCMS\Admin\Repositories\ModuleRepository
     */
    protected $module;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->module = $this->laravel['modules'];

        if ($module_name = $this->option('module')) {
            return $this->rollbackModule($module_name);
        }

        $filesystem = $this->laravel['files'];

        // Delete migrations
        $path = realpath(__DIR__.'/../../../../src/migrations/');

        if ($migrations = $filesystem->files($path)) {
            // Disable foreign key checks
            Schema::disableForeignKeyConstraints();

            foreach ($filesystem->files($path) as $migration) {
                $file = $filesystem->basename($migration);
                $migration_path = $this->laravel['path.database'] . '/migrations/' . $file;

                if ($filesystem->isFile($migration_path)) {
                    $basename = trim($file, '.php');
                    $this->line('<info>Rolling back:</info> ' . $basename);

                    $this->callSilent('migrate:rollback', [
                        '--path' => $migration_path,
                        '--realpath' => true
                    ]);

                    if ($filesystem->delete($migration_path)) {
                        $this->line('<info>Removed:</info> ' . $basename);
                    }
                }
            }

            // Re-enable foreign key constraints check
            Schema::enableForeignKeyConstraints();
        }

        // Remove modules
        foreach ($this->module->all() as $module) {
            $this->rollbackModule($module_name);
        }

        $this->info('Rollback complete.');
    }

    /**
     * Do module migration.
     */
    protected function rollbackModule($name)
    {
        $module = $this->module->findOrFail($name);

        // Get migration path
        $path = $this->getPath($module);

        if ($path = realpath(base_path($path))) {
            // Get migration files
            $filesystem = $this->laravel['files'];
            $files = $filesystem->glob($path . '/*.php');

            // Disable foreign key checks
            Schema::disableForeignKeyConstraints();

            foreach ($files as $file) {
                echo 'Removing file ', $file, '...';
                if (preg_match('/(\d{4}_\d{2}_\d{2}_\d{6}_\w+)\.php/', $file, $match)) {
                    $class = $this->getMigrationClass($file);
                    if (!$class) {
                        continue;
                    }

                    // Remove migration file
                    if (app()['db']->table('migrations')->where('migration', $match[1])->delete()) {
                        include $file;

                        $migration = new $class();
                        $migration->down();
                    }
                }
            }

            // Re-enable foreign key constraints check
            Schema::enableForeignKeyConstraints();

            $this->info(ucwords($name) . ' module migration entry removed.');
        }
    }

    /**
     * Get migration path for specific module.
     *
     * @param  \OroCMS\Admin\Module $module
     * @return string
     */
    protected function getPath($module)
    {
        $path = $module->getExtraPath(config('admin.modules.migration.path'));
        
        return str_replace(base_path(), '', $path);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['module', 'm', InputOption::VALUE_REQUIRED, 'Indicates which module to migrate.'],
        ];
    }

    /**
     * Get declared classes.
     */
    private function getMigrationClass($file)
    {
        $fp = fopen($file, 'r');
        $class = null;

        $i = 0;
        $buffer = '';
        while (!$class) {
            if (feof($fp)) {
                break;
            }

            $buffer .= fread($fp, 1024);
            if (preg_match('/class\s+(\w+)(.*)\{?/', $buffer, $matches)) {
                $class = $matches[1];
                break;
            }
        }

        return $class;
    }
}
