<?php
namespace OroCMS\Admin\Console;

use FilesystemIterator;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeModuleCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:makemodule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create module boilerplate for the OroCMS Admin.';

    /**
     * @var OroCMS\Admin\Repositories\ModuleRepository
     */
    protected $module;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->module = $this->laravel['modules'];

        if ($module_name = $this->option('module')) {
            if ($this->module->find($module_name)) {
                return $this->error("Module {$module_name} already exists.\n");
            }
        } else {
            return $this->error("Module name not specified.\n");
        }

        $path = base_path('modules');

        $filesystem = $this->laravel['files'];
        if (!$filesystem->isDirectory($path)) {
            // Attempt to create the folder
            if ($filesystem->makeDirectory($path)) {
                $this->line("<comment>{$path}</comment> directory created.\n");
            } else {
                return $this->error("Cannot create destination folder \"{$path}\".\n");
            }
        }

        // Create boilerplate
        $this->generateModule($module_name, $path);
    }

    /**
     * Create the module boilerplate.
     *
     * @param  string $module_name
     * @param  string $path
     */
    protected function generateModule($module_name)
    {
        $exclude_files = [
            '.gitignore',
            '.gitkeep'
        ];
        $fillable_files = ['Controller', 'ServiceProvider'];

        $filesystem = $this->laravel['files'];

        // Get module name references
        $module_class = Str::studly($module_name);
        $module_alias = strtolower($module_class);

        $targetPath = realpath(base_path('modules'));

        // Get module stub folder
        $path = realpath(__DIR__.'/../Stubs/module');

        // Get stub files/folders
        $items = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST
        );

        try {
            foreach($items as $sourceName => $item){
                $targetFile = str_replace($path, $targetPath . '/' . $module_class, $sourceName);

                if ($item->isFile()) {
                    if (in_array($item->getBasename(), $exclude_files)) {
                        $filesystem->copy($sourceName, $targetFile);
                    } else {
                        $content = file_get_contents($sourceName);

                        $content = str_replace('%%module_name%%', $module_name, $content);
                        $content = str_replace('%%module_class%%', $module_class, $content);
                        $content = str_replace('%%module_alias%%', $module_alias, $content);

                        $fileName = $filesystem->name($sourceName);
                        if (in_array($fileName, $fillable_files)) {
                            $targetFile = $filesystem->dirname($targetFile) . '/' . $module_class . $fileName . '.' . $filesystem->extension($sourceName);
                        }

                        // Copy modified stub content
                        file_put_contents($targetFile, $content);
                    }
                } else if ($item->isDir()) {
                    $filesystem->makeDirectory($targetFile, 0755, true);
                }
            }

            $this->info("Module scaffold for module \"{$module_name}\" successfully created.");
            $this->line("Target module path is at <comment>{$targetPath}/${module_class}</comment>.");
        } catch (\Exception $e) {
            $this->error('An error has occured. ' . $e->getMessage());

            // Revert/remove base module folder
            $basePath = realpath($path . '/' . $module_class);
            if ($filesystem->isDirectory($basePath)) {
                $this->info('Reverting...');
                
                if ($this->deleteDirectory($basePath)) {
                    $this->line("<comment>{$basePath}</comment> removed.");
                } else {
                    $this->error("Error deleting folder: \"{$basePath}\".");
                }
            }
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['module', 'm', InputOption::VALUE_REQUIRED, 'Indicates the name of the module to create.'],
        ];
    }
}
