<?php
namespace OroCMS\Admin\Validation\Users;

use OroCMS\Admin\Validation\Validator;

class Create extends Validator
{
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:20',
        ];
    }
}
