<?php
namespace OroCMS\Admin\Listeners;

use Illuminate\Foundation\Application;
use OroCMS\Admin\Entities\Plugin as PluginEntity;

class PluginsListener
{
    /**
     * The constructor.
     *
     * @param Application $app
     * @param $name
     * @param $path
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle the specified event.
     */
    public function handle($plugin)
    {
        if (is_string($plugin)) {
            $arr = explode('.', $plugin);
            if ($arr[1]) {
                $plugin = $this->app['plugins']->find($arr[1]);
            }
        }

        if ($plugin instanceof \OroCMS\Admin\Plugin) {
            $hooks = $plugin->getHooks();

            foreach ($hooks as $hook) {
                \Event::listen($hook->event, $hook->class);
            }
        }
    }
}
