<?php
namespace Modules\%%module_class%%\Repositories;

use Modules\%%module_class%%\Entities\Entity;
use Modules\%%module_class%%\Transformers\EntityTransformer;
use OroCMS\Admin\Contracts\EntityRepositoryInterface;
use League\Fractal\Manager;
use League\Fractal\Resource;
use Illuminate\Support\Facades\Request;

class EntityRepository implements EntityRepositoryInterface
{
    /**
     * @var integer
     */
    protected $perPage;

    /**
     * Get model.
     *
     * @param boolean
     *
     * @return \Modules\Articles\Entities\Entity
     */
    public function getModel()
    {
        return Entity::getModel();
    }

    /**
     * Store data.
     *
     * @param array
     */
    public function create(array $data = null)
    {
    }

    /**
     * Update article.
     *
     * @param array
     */
    public function update($id)
    {
    }

    /**
     * Delete article.
     *
     * @param integer
     * @param boolean
     */
    public function delete($cids, $force_delete = false)
    {
        $results = $this->getModel(true)->whereIn('id', $cids);
        $force_delete ? $results->forceDelete() : $results->delete();

        return true;
    }

    /**
     * Return item collection.
     *
     * @return mixed
     */
    public function getAll()
    {
        // build
        $repository = $this->getModel();

        $total = $repository->count();
        $objects = $repository
            ->orderBy($sort, $sort_dir)
            ->take($this->perPage())
            ->skip($this->perPage() * ($page - 1));

        //
        // serialize
        //
        $resource = new Resource\Collection($objects->get(), new EntityTransformer());
        $rows = (new Manager())->createData($resource)->toArray();

        // get listing count
        $count = count($resource->getData());

        return compact('total', 'count') + $rows;
    }

    /**
     * Get entry by id.
     *
     * @param integer
     *
     * @return mixed
     */
    public function findById($id)
    {
        $entry = $this->getModel()->findorFail($id);

        return $entry;
    }

    /**
     * Get entry by key.
     *
     * @param string
     * @param mixed
     * @param string
     *
     * @return mixed
     */
    public function findBy($key, $value, $operator = '=')
    {
        return $this->paginate($this->getModel()->where($key, $operator, $value));
    }

    /**
     * Get page listing count.
     *
     * @return integer
     */
    public function perPage()
    {
        $perPage = (int)Request::get('limit', $this->perPage);
        $perPage = $perPage ?: config('%%module_alias%%.pages.%%module_alias%%.perpage', 10);

        return $perPage;
    }

    /**
     * Return paginated listing
     *
     * @param mixed
     *
     * @return mixed
     */
    public function paginate($data)
    {
        return $data->paginate( $this->perPage() );
    }
}
