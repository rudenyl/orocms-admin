@extends('admin::layouts.master')
@section('title'){{ trans('%%module_alias%%::admin.header') }}@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> {{ trans('admin.menu.dashboard') }}</a>
        </li>
        <li class="active">
            {{ trans('%%module_alias%%::admin.header') }}
        </li>
    </ol>
@stop

@section('content')
    <div class="header-group">
        <div>
            <h1 class="page-header">
                {{ trans('%%module_alias%%::admin.header') }}
            </h1>
        </div>
    </div>

    <div id="content-wrapper" class="list-container module section">
        Admin content for the %%module_name%% module.
    </div>
@stop
