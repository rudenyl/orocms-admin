@extends('theme::_layouts.base')

@if(isset($error))
    @section('title'){{ '403' }}@stop

    @section('content')
        <h1>
            403
        </h1>

        {{ $error }}
    @stop
@endif
