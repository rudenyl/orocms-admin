@extends('theme::_layouts.base')

@section('title')
    %%module_name%%
@stop

@section('content')
    <h1>
        %%module_name%%
    </h1>

    %%module_name%% content.
@stop
