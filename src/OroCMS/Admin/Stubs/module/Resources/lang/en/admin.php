<?php

return [
    'menu' => '%%module_name%%',

    'header' => 'Manage %%module_name%%',
    'breadcrumb' => 'Manage %%module_name%%'
];