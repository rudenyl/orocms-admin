<?php
/**
 * Routes
 */

Route::group(['namespace' => 'Modules\%%module_class%%\Http\Controllers', 'middleware' => 'web'], function() {
    Route::group(['prefix' => config('modules.configs.%%module_alias%%.route.prefix', '%%module_alias%%')], function() {
        Route::get('/', ['as' => '%%module_alias%%.index', 'uses' => '%%module_class%%Controller@index']);
    });

    /**
     * Admin routes
     */
    Route::group([
            'as' => 'admin.%%module_alias%%.',
            'namespace' => 'Admin',
            'prefix' => config('modules.configs.%%module_alias%%.route.cp', '%%module_alias%%'),
            'middleware' => config('admin.filter.auth')], function() {

        Route::get('/', ['as' => 'index', 'uses' => '%%module_class%%Controller@index']);
    });
});
