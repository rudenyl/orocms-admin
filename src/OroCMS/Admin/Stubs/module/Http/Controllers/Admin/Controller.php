<?php
namespace Modules\%%module_class%%\Http\Controllers\Admin;

use OroCMS\Admin\Controllers\BaseController;
use Modules\%%module_class%%\Repositories\EntityRepository;
use Modules\%%module_class%%\Validation\Create;
use Modules\%%module_class%%\Validation\Update;
use Illuminate\Http\Request;

class %%module_class%%Controller extends BaseController
{
    protected $view_prefix = '%%module_alias%%';
    protected $theme = '';

    /**
     * @param Modules\%%module_class%%\Repositories\EntityRepository $repository
     */
    function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        // do something here!

        return $this->view('admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Create $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update(Update $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy(Request $request, $id = null)
    {
    }
}
