<?php 
namespace Modules\%%module_class%%\Http\Controllers;

use Gate;
use Modules\%%module_class%%\Repositories\EntityRepository;
use OroCMS\Admin\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class %%module_class%%Controller extends BaseController 
{
    use AuthorizesRequests;

    protected $route_prefix = '%%module_alias%%';
    protected $view_prefix = '%%module_alias%%';
    protected $theme = '';

    protected $repository;

    function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->view('index');
    }
}
