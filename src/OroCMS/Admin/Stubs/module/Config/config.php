<?php

return [
    'name' => '%%module_name%%',
    'route' => [
        'prefix' => '%%module_alias%%',
        'cp' => 'admin/%%module_alias%%'
    ]
];