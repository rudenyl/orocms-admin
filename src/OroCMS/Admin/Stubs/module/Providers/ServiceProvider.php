<?php
namespace Modules\%%module_class%%\Providers;

use Caffeinated\Menus\Facades\Menu;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class %%module_class%%ServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Boot the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerViews();
		$this->registerComposers();
		$this->registerListeners();
		$this->registerMenu();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerConfig();
	}

    /**
     * Attach to admin menu.
     *
     * @return void
     */
    public function registerMenu()
    {
        if ($menu = Menu::get('admin')) {
            // plugins
            $menu->insert(1, trans('%%module_alias%%::admin.menu'), route('admin.%%module_alias%%.index'))
                ->data('glyphicon', 'glyphicon glyphicon-file')
                ->active('admin/%%module_alias%%/*');
        }
    }

	/**
	 * Register config.
	 *
	 * @return void
	 */
	protected function registerConfig()
	{
		$configPath = __DIR__.'/../Config/config.php';
        if (file_exists($configPath)) {
            $this->mergeConfigFrom($configPath, 'modules.configs.%%module_alias%%');
        }
	}

	/**
	 * Register views.
	 *
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/%%module_alias%%');
		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath,

		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], '%%module_alias%%');
	}

	/**
	 * Register translations.
	 *
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/%%module_alias%%');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, '%%module_alias%%');
		}
		else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', '%%module_alias%%');
		}
	}

	/**
	 * Register view composers.
	 *
	 * @return void
	 */
	public function registerComposers()
	{
		view()->composer('%%module_alias%%::admin.form', function($view) {
		    #
		    # onBeforeRenderItem
		    #
		    if ($view->offsetExists('model')) {
		        $model = $view->offsetGet('model');

		        event('%%module_alias%%.admin.onBeforeRenderItem', $model);
		    }

		    #
		    # onAfterRenderItem
		    #
	        event('%%module_alias%%.admin.onAfterRenderItem', $view);
		});
	}

	/**
	 * Register listeners.
	 *
	 * @return void
	 */
	public function registerListeners()
	{
		// Register your listeners here
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

}
