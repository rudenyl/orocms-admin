<?php
namespace Modules\%%module_class%%\Transformers;

use Hashids\Hashids;
use Modules\%%module_class%%\Entities\Entity;
use League\Fractal\TransformerAbstract;

class EntityTransformer extends TransformerAbstract
{

    public function transform(Entity $entity)
    {
        // Sample only
        return [
            'col1' => $entity->col1,
            'col2' => $entity->col2
        ];
    }
}