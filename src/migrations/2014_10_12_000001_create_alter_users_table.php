<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                $table->boolean('published');
                $table->timestamp('last_login')->nullable();
                $table->softDeletes();
            });
        } else {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('firstname', 80);
                $table->string('lastname', 80);
                $table->string('email')->unique();
                $table->string('password', 60);
                $table->boolean('published');
                $table->rememberToken();
                $table->timestamp('last_login')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'published') && Schema::hasColumn('users', 'last_login') &&
            Schema::hasColumn('users', 'deleted_at')
        ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn(['published', 'last_login', 'deleted_at']);
            });
        }
        else {
            Schema::drop('users');
        }
    }
}