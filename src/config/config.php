<?php

return [
    'prefix' => 'admin',
    'filter' => [
        'auth' => [
            OroCMS\Admin\Middleware\Authenticate::class,
            OroCMS\Admin\Middleware\AdminOnly::class
        ],
        'guest' => OroCMS\Admin\Middleware\RedirectIfAuthenticated::class,
    ],
    'models' => [
        'user' => OroCMS\Admin\Entities\User::class,
        'role' => OroCMS\Admin\Entities\Role::class
    ],
    'views' => [
        'layout' => 'admin::layouts.master',
    ],
    'modules' => [
        'path' => base_path('modules'),
        'migration' => [
            'path' => 'Database/Migrations'
        ],
        'lang' => [
            'path' => 'Resources/lang',
            'default_locale' => 'en'
        ]
    ],
    'plugins' => [
        'path' => base_path('plugins')
    ],
    'themes' => [
        // Comment out the next 2 lines if you want to display the default views
        // This will then discard the frontend views themeable feature
        'path' => base_path('resources/views/themes'),
        'default_theme' => 'default',

        'cp' => [
            'default_theme' => 'bootstrapped'
        ]
    ],
    // Un-comment the lines below if you want to manage the users
    // in a separate module
    // 'management' => [
    //     'users' => false
    // ]
];
